import SwiftUI

struct AnimationConfig {
    var rotationDegrees: Double
    var shouldFade: Bool
    var scale: CGSize
}

struct ContentView: View {
    @State private var showingScore = false
    @State private var showingFinal = false
    @State private var scoreTitle = ""
    @State private var scoreMessage = ""
    @State private var userScore: Int = .zero
    @State private var round: Int = 1
    @State private var animationMatrix: [String: AnimationConfig] = [:]

    @State var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"].shuffled()
    @State var correctAnswer = Int.random(in: 0...2)

    var body: some View {
        ZStack {
            RadialGradient(stops: [
                .init(color: Color(red: 0.1, green: 0.2, blue: 0.45), location: 0.3),
                .init(color: Color(red: 0.76, green: 0.15, blue: 0.3), location: 0.3)
            ], center: .top, startRadius: 150, endRadius: 250)
                .ignoresSafeArea()

            VStack {
                Spacer()

                Text("Guess the flag")
                    .font(.largeTitle.bold())
                    .foregroundColor(.white)

                VStack(spacing: 15) {
                    VStack {
                        Text("Tap the flag of")
                            .foregroundStyle(.secondary)
                            .font(.subheadline.weight(.heavy))

                        Text(countries[correctAnswer])
                            .font(.largeTitle.weight(.semibold))
                    }

                    ForEach(0..<3) { number in

                        let fade = animationMatrix[countries[number]]?.shouldFade ?? false
                        let rotation = animationMatrix[countries[number]]?.rotationDegrees ?? .zero
                        let scale: CGSize = animationMatrix[countries[number]]?.scale ?? CGSize(width: 1, height: 1)

                        Button {
                            flagTapped(number)
                        } label: {
                            Image(countries[number])
                                .renderingMode(.original)
                                .clipShape(Capsule())
                                .shadow(radius: 5)
                        }
                        .opacity(fade ? 0.25 : 1)
                        .rotation3DEffect(.degrees(rotation), axis: (x: 0, y: 1, z: 0))
                        .scaleEffect(scale)
                        .animation(.easeIn(duration: fade ? 0.5 : .zero), value: scale)
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical, 20)
                .background(.thinMaterial)
                .clipShape(RoundedRectangle(cornerRadius: 20))

                Spacer()
                Spacer()

                Text("Score: \(userScore)")
                    .foregroundColor(.white)
                    .font(.title.bold())

                Spacer()
            }
            .padding(20)
        }
        .alert(scoreTitle, isPresented: $showingScore) {
            Button("Continue", action: askQuestion)
        } message: {
            Text("\(scoreMessage) Your score is \(userScore)")
        }

        .alert("Final Score", isPresented: $showingFinal) {
            Button("Play Again", action: reset)
        } message: {
            Text("Final score is \(userScore)!")
        }
    }

    func flagTapped(_ number: Int) {
        withAnimation {
            for (index, _) in countries.enumerated() {
                animationMatrix[countries[index]] = .init(rotationDegrees: index == number ? 360 : .zero,
                                                          shouldFade: index != number,
                                                          scale: index == number
                                                          ? .init(width: 1, height: 1)
                                                          : .init(width: 0.3, height: 0.3))
            }
        }

        if number == correctAnswer {
            scoreTitle = "Correct"
            userScore += 1
        } else {
            scoreTitle = "Wrong"
            if userScore > .zero {
                userScore -= 1
            }
        }

        round += 1
        scoreMessage = "That's the flag of \(countries[number])."
        showingScore = true
    }

    func askQuestion() {
        animationMatrix.removeAll()

        if round == 9 {
            showingFinal = true
        } else {
            countries.shuffle()
            correctAnswer = Int.random(in: 0...2)
        }
    }

    func reset() {
        countries.shuffle()
        showingFinal = false
        round = 1
        userScore = .zero
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
    }
}
