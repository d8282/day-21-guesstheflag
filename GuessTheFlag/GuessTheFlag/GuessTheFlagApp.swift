//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Drummer on 2022. 05. 01..
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
